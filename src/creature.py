import numpy as np
import random as rng
import pygame.draw as draw

WHITE = (255,255,255)

class Creature:
    def __init__(self, population, size, x = 0, y = 0, color = WHITE):
        self.pop = population
        self.repro = .2
        self.mut = .01
        self.crowding = .0001
        self.death = .05
        self.color = color
        self.pos = (x,y)
        self.size = size
        self.moveTimer = rng.randint(0,30)
        self.moveCounter = 0
        self.moveDelta = (0,0)

    def move(self, speed):
        #set new delta
        if self.moveCounter == self.moveTimer:
            x = rng.uniform(-1, 1) * speed
            y = rng.uniform(-1, 1) * speed
            self.moveDelta = (x,y)
            self.moveCounter = 0

        #move
        x = self.moveDelta[0] + self.pos[0]
        y = self.moveDelta[1] + self.pos[1]

        if x < 0:
            x = 0
        elif x > 500:
            x = 500

        if y < 0:
            y = 0
        elif x > 500:
            y = 500

        self.pos = (x,y)

        self.moveCounter += 1

    def timeStep(self):
        #print(len(self.list))
        if np.random.random() < self.repro:
            if np.random.random() < self.mut:
                self.mutate()
            else:
                self.reproduce()

        if np.random.random() < (self.death + self.crowding * len(self.pop.creatures)):
            self.kill()

    def kill(self):
        self.pop.killCreature(self)

    def reproduce(self):
        c = Creature(self.pop, self.size, color=self.color)
        c.repro = self.repro
        c.crowding = self.crowding
        c.death = self.death
        self.pop.addCreature(self.pos, c)

    def mutate(self):
        repro = self.repro + rng.uniform(-.1, .1)
        crowding = self.crowding+ rng.uniform(-.0001, .0001)
        death = self.death + rng.uniform(-.01, .01)
        color = self.genColor(self.color)

        c = Creature(self.pop, self.size, color=color)

        choice = rng.choice([0,1,2])
        if choice == 0:
            c.repro = repro
        elif choice == 1:
            c.crowding = crowding
        else:
            c.death = death

        self.pop.addCreature(self.pos, c)

    def genColor(self, color):
        colors = [color[0],color[1],color[2]]
        newColors = []
        for c in colors:
            c += rng.randint(-100,100)
            if c > 255:
                c = 255
            elif c  < 0:
                c = 0
            newColors.append(c)

        return (newColors[0], newColors[1], newColors[2])

    def draw(self, surface):
        draw.rect(surface, self.color, (self.pos[0],self.pos[1],self.size,self.size))
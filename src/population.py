import random as rng
from creature import Creature

class Population:

    def __init__(self):
        self.creatures = []
    
    def setCreature(self, creature, initPop = 50):
        self.creatures.append(creature)
        
        for i in range(initPop):
            parent = self.creatures[-1]
            self.addCreature(parent.pos, Creature(self, parent.size))

    def timeStep(self):
        stepList = self.creatures
        print(len(stepList))
        for c in stepList:
            c.timeStep()

    def update(self):
        for c in self.creatures:
            c.move(1)

    def draw(self, WIN):
        for c in self.creatures:
            c.draw(WIN)

    def addCreature(self, parentPos, creature):
        if len(self.creatures) < 10000:
            x = rng.choice([0,1,-1])
            y = rng.choice([0,1,-1])
            while x == 0 and y ==0:
                x = rng.choice([0,1,-1])
                y = rng.choice([0,1,-1])
            creature.pos = (parentPos[0] + x * creature.size, parentPos[1] + y * creature.size)
            self.creatures.append(creature)

    def killCreature(self, id):
        self.creatures.remove(id)
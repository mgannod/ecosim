import pygame
from pygame import display
from pygame import time
from pygame import event
from creature import Creature
from population import Population
import random as rng

SIZE = 500

rng.seed()
pygame.init()
winSize = (SIZE,SIZE)
WIN = display.set_mode(winSize)
FPS = 60
BLACK = (0,0,0)

TIMESTEP = pygame.USEREVENT+1

time.set_timer(TIMESTEP, 1000)

pop = Population()
pop.setCreature(Creature(pop, 5, rng.randint(200,300), rng.randint(200,300)))

def main():
    #set clock
    clock = time.Clock()

    run = True
    while run:
        clock.tick(FPS)#keep 60 fps
        #check events
        for events in event.get():
            if events.type == pygame.QUIT:#quit on window close
                run = False
            if events.type == TIMESTEP:
                pop.timeStep()

        pop.update()

        #update board and draw chagnes
        draw_window()

    pygame.quit()

def draw_window():
    WIN.fill(BLACK)
    pop.draw(WIN)
    pygame.display.update() #draww screen

if __name__ == '__main__':
    main()